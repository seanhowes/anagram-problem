#!/usr/bin/python

# Author : Sean Howes
#
# For testing I used /usr/share/dict/american-english
# I settled on the prime numbers approach after going through an intial 
# approach which was to group words by the sum of there chars values
# then for each group sort and compare adding the matching to a new list.
# It was wasteful in both storage and processing so I settled on this approach.
# 
# This approach has one flaw in that it can only deal the english char set
# though the letter map could be expanded for non english chars or replaced
# with a method that would give you nth prime where n is the uni code value 
# for a given char. I felt this would be slower and a bit of over kill for this


import sys
# should change to method that returns prime for char 
letterMap = {'a':2,'b':3,'c':5,'d':7,'e':11,'f':13,'g':17,'h':19,
	     'i':23,'j':29,'k':31,'l':37,'m':41,'n':43,'o':47,'p':53,
	     'q':59,'r':61,'s':67,'t':71,'u':73,'v':79,'w':83,'x':89,
             'y':97,'z':101,"'":103}
ana ={}
if len(sys.argv) < 2:
    print "Usage : anagram.py /path/to/word/list/file"
    sys.exit(2)
else: 
    #using with as from docs it handels file close for me
    with open(sys.argv[1],'r') as f: 
        word = f.readline()
        while word:
	    word = word.lower().rstrip()
	    wordAsList = list(word)
	    score = 1
	    # calcuate product of primes for letters in word
	    for letter in wordAsList:
                # use default value of 1 so chars not in map ignored 
	        score *= letterMap.get(letter,1) 
	    if score not in ana:
                ana[score] = [] 
            # make sure we dont insert duplicate 
	    if word not in ana[score]:
                ana[score].append(word) 
            word = f.readline()
  
for key in ana:
     if len(ana[key]) > 1:
	 print  ' ,'.join(ana[key])

